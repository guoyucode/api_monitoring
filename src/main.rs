#[macro_use]
extern crate log;

mod request;

use std::thread::sleep;
use std::time::Duration;

fn main() {
    let _watch = common_uu::toml_read::run_watch("config.toml").unwrap();
    common_uu::mylog::init_log("api_monitoring");
    loop {
        let sleep_time = common_uu::arg::get_arg("time.sleep").map(|v| v.parse::<u64>().unwrap_or_else(|_| 5000)).unwrap_or_else(|| 5000);
        sleep(Duration::from_millis(sleep_time));
        request::run();
    }
}
